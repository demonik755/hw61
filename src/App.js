import React, {Component} from 'react';
import CountryList from "./components/CountryList";

import  './App.css';

class App extends Component {

    render() {

      return (
          <div className="App">
                <h1>Search Country</h1>
              <CountryList/>

          </div>
      );
    }
}

export default App;