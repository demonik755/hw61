import React, {Component, Fragment} from 'react';
import axios from "axios";
import './countryList.css';
class CountryList extends Component {
    state = {
        countryInfo: null,
        countries: []
    };
    componentDidMount() {
        axios.get('https://restcountries.eu/rest/v2/all').then(response => {
            this.setState({countries: response.data});
        });
    }
    searchCountry = (event) => {
            axios.get(`https://restcountries.eu/rest/v2/name/${event.target.value}`).then(response => {
               this.setState ({countryInfo: response.data[0]});
            })
    };
    render() {
        const { countries } = this.state;
        let countriesList = countries.length > 0
            && countries.map((item, i) => {
                return (
                    <option key={i} value={item.id}>{item.name}</option>
                )
            }, this);
        return (
                <Fragment>
                    <div className="countryInfo">{this.state.countryInfo? <img src={this.state.countryInfo.flag} width="350" height="250" alt="flag"/> :null}</div>
                    <select className="countryList"  onChange={(event)=>this.searchCountry(event)}>
                     {countriesList}
                   </select>
                    <div className="countryInfo">Население: {this.state.countryInfo? this.state.countryInfo.population:null}</div>
                    <div className="countryInfo">Регион: {this.state.countryInfo? this.state.countryInfo.region:null}</div>
                    <div className="countryInfo">Граничит с: {this.state.countryInfo && this.state.countryInfo.borders.join(" ,")}</div>
                    <div className="countryInfo"> Столица: {this.state.countryInfo? this.state.countryInfo.capital:null}</div>
                </Fragment>
        );
    }
}

export default CountryList;